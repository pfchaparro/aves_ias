import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class AveService {
  private url_base: string = 'https://apex.oracle.com/pls/apex/pfchaparro1/v1/';
  private url_ave: string = 'aves';
  private url_pais: string = 'pais';
  private url_zone: string = 'zonas';

  constructor(private http: HttpClient) { }

  public listAve(): Observable<any>{
    return this.http.get(this.url_base + this.url_ave);
  }

  public postAve(ave: object): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.post(this.url_base + this.url_ave, ave, { headers: headers });
  }

  public deleteAve(id: number): Observable<any> {
    return this.http.delete(this.url_base + this.url_ave + '/' + id);
  }

  public putAve(ave: object, id: number): Observable<any> {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    return this.http.put(this.url_base + this.url_ave + '/' + id, ave, { headers: headers });
  }

  public listCountry(): Observable<any>{
    return this.http.get(this.url_base + this.url_pais);
  }

  public listZone(): Observable<any>{
    return this.http.get(this.url_base + this.url_zone);
  }
}
