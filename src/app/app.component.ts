import { Component, TemplateRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { MatTableDataSource } from '@angular/material/table';
import { AveService } from './services/ave.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  public displayedColumns: string[] = ['cod_ave', 'nombre_comun', 'nombre_cientifico', 'nombre_pais', 'nombre_zona', 'action'];
  public dataSource: any;
  public formGroup: FormGroup;
  public formName: FormGroup;
  public configurationModal: any;
  public modalReference: BsModalRef;
  public selected = '0001';
  public country: any;
  public zone: any;
  public cdpais: any;
  public cdzone: string;
  public nameSearch: string;
  public info: any;

  constructor(private serviceAve: AveService, public fb: FormBuilder, private modalService: BsModalService) {
    this.configurationModal = {
      animated: true,
      keyboard: true,
      backdrop: true,
      ignoreBackdropClick: true
    },
      this.formGroup = this.fb.group({
        dsnombre_comun: ['', [Validators.required, Validators.minLength(5)]],
        dsnombre_cientifico: ['', [Validators.required, Validators.minLength(5)]],
      });

    this.formName = this.fb.group({
      nombre: [],
      zona: []
    });
  }

  ngOnInit() {
    this.getAve();
    this.getZone();
  }

  public infoEdit(ave) {
    this.info = ave;
    console.log(this.info);
  }


  public filtrar = (value: string) => {
    this.dataSource.filter = value.trim().toLocaleLowerCase();
  }

  public selectZone(cdzone) {
    this.cdzone = cdzone;
  }

  public delete(id) {
    let valid = confirm("Esta seguro que desea eliminar este registro?");
    if (valid) {
      let response: any;
      this.serviceAve.deleteAve(id).subscribe(
        data => {
          response = JSON.parse(JSON.stringify(data));
          this.getAve();
          alert('Ave Eliminada correctamente');
        },
        error => {
          alert('Ocurrio un error al eliminar el ave');
          console.log(error);
        },
        () => console.log(response)
      );
    }
  }

  public edit(ObjetAve): void {
    let response: any;
    const ave = this.formGroup.value;
    this.serviceAve.putAve(ave, ObjetAve.cod_ave).subscribe(
      data => {
        response = JSON.parse(JSON.stringify(data));
        this.modalReference.hide();
        alert('Ave modificada correctamente');
        this.clearField();
        this.getAve();
      },
      error => {
        alert('Ocurrio un error al editar el ave');
        console.log(error)
      },
      () => console.log(response)
    );
  }

  public selectCountry(cdpais) {
    this.cdpais = cdpais;
  }

  public postAve(): void {
    let response: any;
    this.formGroup.value.cdave = Math.floor(Math.random() * 10000);
    this.formGroup.value.cdpais = this.cdpais;
    const ave = this.formGroup.value;
    this.serviceAve.postAve(ave).subscribe(
      data => {
        response = JSON.parse(JSON.stringify(data));
        this.modalReference.hide();
        alert('Ave creada correctamente');
        this.clearField();
        this.getAve();
      },
      error => {
        alert('Ocurrio un error al crear el ave.');
      },
      () => console.log(response)
    );
  }

  public clearField() {
    this.formGroup.reset();
  }

  public getAve() {
    this.serviceAve.listAve().subscribe(
      data => {
        this.dataSource = data.items;
        this.dataSource = new MatTableDataSource(this.dataSource)
      },
      err => {
        console.log(err);
        alert('Error al cargar listado de aves');
      }
    );
  }

  public getCountry() {
    this.serviceAve.listCountry().subscribe(
      data => {
        this.country = data.items;
      },
      err => {
        console.log(err);
        alert('Error al cargar listado de paises');
      }
    );
  }

  public getZone() {
    this.serviceAve.listZone().subscribe(
      data => {
        this.zone = data.items;
      },
      err => {
        console.log(err);
        alert('Error al cargar listado de aves');
      }
    );
  }

  public openDialog(template: TemplateRef<any>) {
    this.getCountry();
    this.modalReference = this.modalService.show(template, this.configurationModal);
  }
}
