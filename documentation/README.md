# Aves API
API de aves

# URL GitLab
`https://gitlab.com/pfchaparro/aves_ias.git`.  

# URL Hosting
`https://test-butcket-1997.s3.us-east-2.amazonaws.com/`. 


## GET - Listar Aves

Method : GET  
URL: `https://apex.oracle.com/pls/apex/pfchaparro1/v1/aves`.  
Response:  
~~~
{
  "items": [
    {
      "cod_ave": "AV1",
      "nombre_comun": "AVE 1",
      "nombre_cientifico": "NAME 1",
      "codigo_pais": "CO",
      "nombre_pais": "COLOMBIA",
      "cod_zona": "Z1",
      "nombre_zona": "ZONA 1"
    },
    {
      "cod_ave": "AV3",
      "nombre_comun": "ACTULIZAR 2",
      "nombre_cientifico": "UPDATE 3",
      "codigo_pais": "PE",
      "nombre_pais": "PERU",
      "cod_zona": "Z1",
      "nombre_zona": "ZONA 1"
    }
  ],
  "hasMore": false,
  "limit": 10,
  "offset": 0,
  "count": 2,
  "links": [
    {
      "rel": "self",
      "href": "https://apex.oracle.com/pls/apex/pfchaparro1/v1/aves"
    },
    {
      "rel": "edit",
      "href": "https://apex.oracle.com/pls/apex/pfchaparro1/v1/aves"
    },
    {
      "rel": "describedby",
      "href": "https://apex.oracle.com/pls/apex/pfchaparro1/metadata-catalog/v1/item"
    },
    {
      "rel": "first",
      "href": "https://apex.oracle.com/pls/apex/pfchaparro1/v1/aves"
    }
  ]
}
~~~

## PUT - Actualizar Ave
**Method:** PUT  
**URL:** `https://apex.oracle.com/pls/apex/pfchaparro1/v1/aves`.  
**Body:**  
~~~
{
    "cdpais": "PE",
    "dsnombre_comun": "ACTULIZAR 2",
    "dsnombre_cientifico": "UPDATE 3"
}
~~~

## POST - Crear Ave
**Method:** POST  
**URL:** `https://apex.oracle.com/pls/apex/pfchaparro1/v1/aves`.  
**Body:**  
~~~
{
    "cdpais": "CO",
    "cdave": "AV3",
    "dsnombre_comun": "AVE 3",
    "dsnombre_cientifico": "NOMBRE 3"
}
~~~

## DELETE - Eliminar Ave
**Method:** DELETE  
**URL:** `https://apex.oracle.com/pls/apex/pfchaparro1/v1/aves/{cdave}`.  

## GET - Listar Países
**Method:** GET  
**URL:** `https://apex.oracle.com/pls/apex/pfchaparro1/v1/pais`.  

## GET - Listar Zonas
**Method:** GET  
**URL:** `https://apex.oracle.com/pls/apex/pfchaparro1/v1/zonas`.  